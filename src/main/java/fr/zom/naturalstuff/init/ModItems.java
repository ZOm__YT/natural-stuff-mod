package fr.zom.naturalstuff.init;

import fr.zom.naturalstuff.items.NSItem;
import fr.zom.naturalstuff.utils.Refs;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;

@Mod.EventBusSubscriber(modid = Refs.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModItems
{

	@ObjectHolder(Refs.MODID + ":elemental_gem")
	public static final Item elemental_gem = new NSItem("elemental_gem");

	@ObjectHolder(Refs.MODID + ":water_gem")
	public static final Item water_gem = new NSItem("water_gem");

	@ObjectHolder(Refs.MODID + ":earth_gem")
	public static final Item earth_gem = new NSItem("earth_gem");

	@ObjectHolder(Refs.MODID + ":shadow_gem")
	public static final Item shadow_gem = new NSItem("shadow_gem");

	@ObjectHolder(Refs.MODID + ":nature_gem")
	public static final Item nature_gem = new NSItem("nature_gem");

	@ObjectHolder(Refs.MODID + ":fire_gem")
	public static final Item fire_gem = new NSItem("fire_gem");

	public static final ModItems INSTANCE = new ModItems();

	public static void init()
	{

	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> e)
	{
		init();
		e.getRegistry().register(elemental_gem);
		e.getRegistry().register(water_gem);
		e.getRegistry().register(fire_gem);
		e.getRegistry().register(earth_gem);
		e.getRegistry().register(nature_gem);
		e.getRegistry().register(shadow_gem);
	}


}
