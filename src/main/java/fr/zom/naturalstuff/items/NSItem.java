package fr.zom.naturalstuff.items;

import fr.zom.naturalstuff.Main;
import net.minecraft.item.Item;

public class NSItem extends Item
{
	public NSItem(String name, Properties properties)
	{
		super(properties.group(Main.nsTab));
		setRegistryName(name);
	}

	public NSItem(String name)
	{
		this(name, new Properties());
	}
}
