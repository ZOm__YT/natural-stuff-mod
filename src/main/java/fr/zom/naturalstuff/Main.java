package fr.zom.naturalstuff;

import fr.zom.naturalstuff.config.Config;
import fr.zom.naturalstuff.init.ModBlocks;
import fr.zom.naturalstuff.init.ModItems;
import fr.zom.naturalstuff.utils.Refs;
import fr.zom.naturalstuff.world.OreGeneration;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.LootTableManager;
import net.minecraft.world.storage.loot.LootTables;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;

@Mod(Refs.MODID)
public class Main
{

	public static final Logger LOGGER = LogManager.getLogger(Refs.MODID.toUpperCase());

	public Main()
	{

		ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, Config.server_config);
		ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, Config.client_config);

		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::commonSetup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientSetup);

		Config.loadConfig(Config.server_config, FMLPaths.CONFIGDIR.get().resolve("naturalstuff-server.toml").toString());
		Config.loadConfig(Config.client_config, FMLPaths.CONFIGDIR.get().resolve("naturalstuff-client.toml").toString());

		MinecraftForge.EVENT_BUS.register(ModItems.INSTANCE);
		MinecraftForge.EVENT_BUS.register(ModBlocks.INSTANCE);

	}

	public static final ItemGroup nsTab = new ItemGroup("natural_stuff_tab")
	{
		@Override
		public ItemStack createIcon()
		{
			return new ItemStack(ModItems.elemental_gem);
		}
	};

	public void commonSetup(final FMLCommonSetupEvent e)
	{

		OreGeneration.setupGeneration();
		LOGGER.info("Common setup finished !");
	}


	public void clientSetup(final FMLClientSetupEvent e)
	{
		LOGGER.info("Client setup started !");
		LOGGER.info("Client setup finished !");
	}

}
