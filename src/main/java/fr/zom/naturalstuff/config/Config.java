package fr.zom.naturalstuff.config;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import fr.zom.naturalstuff.Main;
import net.minecraftforge.common.ForgeConfigSpec;

import java.io.File;

public class Config
{
	private static final ForgeConfigSpec.Builder server_builder = new ForgeConfigSpec.Builder();
	public static final ForgeConfigSpec server_config;

	private static final ForgeConfigSpec.Builder client_builder = new ForgeConfigSpec.Builder();
	public static final ForgeConfigSpec client_config;

	static
	{
		OreGenConfig.init(server_builder, client_builder);

		server_config = server_builder.build();
		client_config = client_builder.build();
	}

	public static void loadConfig(ForgeConfigSpec config, String path)
	{
		Main.LOGGER.info("Loading config" + path);
		final CommentedFileConfig file = CommentedFileConfig.builder(new File(path)).sync().autosave().writingMode(WritingMode.REPLACE).build();
		Main.LOGGER.info("Built config: " + path);
		file.load();
		Main.LOGGER.info("Loaded configs: " + path);
		config.setConfig(file);
	}

}
