package fr.zom.naturalstuff.config;

import net.minecraftforge.common.ForgeConfigSpec;

public class OreGenConfig
{

	public static ForgeConfigSpec.IntValue ore_gen_chance;

	public static void init(ForgeConfigSpec.Builder server, ForgeConfigSpec.Builder client)
	{
		server.comment("OreGen Config..");

		ore_gen_chance = server
				.comment("Maximum number of ore veins generated per chunk")
				.defineInRange("oregen.veinschunknumber", 10, 1, 50);
	}
}
